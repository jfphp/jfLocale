# jf/locale

Gestor de las traducciones.

## Instalación

### Composer

Este proyecto usa como gestor de dependencias [Composer](https://getcomposer.org) el cual puede ser instalado siguiendo
las instrucciones especificadas en la [documentación](https://getcomposer.org/doc/00-intro.md) oficial del proyecto.

Para instalar el paquete `jf/locale` usando este manejador de paquetes se debe ejecutar:

```sh
composer require jf/locale
```

#### Dependencias

Cuando el proyecto es instalado, adicionalmente se instalan las siguientes dependencias:

| Paquete   | Versión |
|:----------|:--------|
| jf/assert | ^3.1    |
| jf/base   | ^4.0    |

### Control de versiones

Este proyecto puede ser instalado usando `git`. Primero se debe clonar el proyecto y luego instalar las dependencias:

```sh
git clone git@gitlab.com:jfphp/jfLocale.git
cd jfLocale
composer install
```

## Archivos disponibles

### Clases

| Nombre                               | Descripción                                                                                       |
|:-------------------------------------|:--------------------------------------------------------------------------------------------------|
| [jf\Locale\Compile](src/Compile.php) | Compila los archivos de traducción gestionados por `gettext`.                                     |
| [jf\Locale\Domains](src/Domains.php) | Genera el archivo `domains.yaml` con la información de los dominios para los idiomas encontrados. |
| [jf\Locale\Locale](src/Locale.php)   | Gestiona la localización de aplicaciones usando la extensión `gettext`.                           |
| [jf\Locale\Update](src/Update.php)   | Actualiza los archivos de traducción gestionados por `gettext`.                                   |

### Traits

| Nombre                                     | Descripción                                        |
|:-------------------------------------------|:---------------------------------------------------|
| [jf\Locale\TFormatter](src/TFormatter.php) | Gestiona los formateadores de la extensión `Intl`. |

## Scripts
