<?php

//require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../../autoload.php';

function checkdir(string $directory) : void
{
    if (!is_file("$directory/composer.json"))
    {
        die("El directorio `$directory` no parece la raíz de un proyecto PHP\n");
    }
}

function localedir(string $directory) : string
{
    $localedir = "$directory/locale";
    if (!is_dir($localedir) && !mkdir($localedir, 0o777, TRUE))
    {
        die("El directorio `$localedir` no existe\n");
    }

    return $localedir;
}

function scan(array|string $directory) : array
{
    $files = [];
    if (is_array($directory))
    {
        foreach ($directory as $dir)
        {
            $files = [ ...$files, ...scan($dir) ];
        }
    }
    elseif (is_dir($directory))
    {
        foreach (glob(rtrim($directory, '/') . '/*') ?: [] as $item)
        {
            if (is_dir($item))
            {
                array_push($files, ...scan($item));
            }
            elseif (str_ends_with($item, '.php'))
            {
                $files[] = $item;
            }
        }
    }
    sort($files);

    return $files;
}
