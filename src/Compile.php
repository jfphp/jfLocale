<?php

namespace jf\Locale;

use jf\assert\Assert;

/**
 * Compila los archivos de traducción gestionados por `gettext`.
 */
class Compile
{
    /**
     * Directorio con las traducciones del proyecto principal.
     *
     * @var string
     */
    private readonly string $_directory;

    /**
     * Opciones para ejecutar el comando `msgfmt`.
     *
     * @var string[]
     */
    public array $options = [ 'check' ];

    /**
     * Constructor de la clase.
     *
     * @param string $directory Directorio con las traducciones del proyecto principal.
     */
    public function __construct(string $directory)
    {
        Assert::isDir($directory);
        $this->_directory = realpath($directory);
    }

    /**
     * Compila los archivos de traducciones existentes en el directorio de entrada.
     *
     * @return array<string,bool> Listado de archivos procesados y su estado.
     */
    public function compile() : array
    {
        $pofiles = glob("$this->_directory/*/*/*.po");
        Assert::notEmpty($pofiles, dgettext('locale', 'No se encontraron archivos .po para compilar'));
        $cmd = 'msgfmt';
        foreach ($this->options as $option => $value)
        {
            if (is_int($option))
            {
                $option = $value;
                $value  = NULL;
            }
            if ($option[0] !== '-')
            {
                $option = strlen($option) > 1
                    ? "--$option"
                    : "-$option";
            }
            $cmd .= $value === NULL
                ? " $option"
                : (" $option=" . escapeshellarg($value));
        }
        $files = [];
        foreach ($pofiles as $pofile)
        {
            $mofile = preg_replace('/\.po$/', '.mo', $pofile) ?: '';
            passthru(
                sprintf(
                    '%s -o %s %s',
                    $cmd,
                    escapeshellarg($mofile),
                    escapeshellarg($pofile)
                ),
                $code
            );
            $files[ $mofile ] = $code === 0;
        }

        return $files;
    }
}