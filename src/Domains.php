<?php

namespace jf\Locale;

use jf\assert\Assert;
use jf\Base\File\Yaml;
use jf\Base\TDasherize;

/**
 * Genera el archivo `domains.yaml` con la información de los dominios para
 * los idiomas encontrados.
 */
class Domains extends Yaml
{
    use TDasherize;

    /**
     * Directorio con las traducciones del proyecto principal.
     *
     * @var string
     */
    private readonly string $_directory;

    /**
     * Listado de dominios encontrados.
     *
     * @var array<string,string>
     */
    private array $_domains = [];

    /**
     * Constructor de la clase.
     *
     * @param string $directory Directorio con las traducciones del proyecto principal.
     */
    public function __construct(string $directory)
    {
        Assert::isDir($directory);
        $directory        = realpath($directory);
        $this->_directory = $directory;
        $this->add([ $directory ]);
    }

    /**
     * Agrega los dominios encontrados en el directorio y que contienen traducciones.
     *
     * @param string[] $rootdirs Listado de directorio donde buscar las traducciones.
     *                           Deberían ser los `{root}/locale` de cada proyecto.
     *
     * @return void
     */
    public function add(array $rootdirs) : void
    {
        $subdir = '/' . basename($this->_directory);
        $len    = strlen($subdir);
        foreach ($rootdirs as $rootdir)
        {
            foreach (glob("$rootdir/*/*/*.mo") as $mofile)
            {
                $this->_domains[ $this->build($mofile) ] = str_starts_with($mofile, 'vendor/')
                    ? '../' . substr($mofile, 0, strpos($mofile, $subdir) + $len)
                    : '.';
            }
        }
    }

    /**
     * Construye el nombre del dominio a usar para el archivo con la traducción.
     *
     * @param string $file Ruta del archivo con la traducción.
     *
     * @return string
     */
    public function build(string $file) : string
    {
        return pathinfo($file, PATHINFO_FILENAME);
    }

    /**
     * Devuelve el dominio a usar por defecto.
     *
     * @return string
     */
    public function default() : string
    {
        return array_search('.', $this->_domains) ?: '';
    }

    /**
     * Devuelve el listado de dominios encontrados.
     *
     * @return array<string,string>
     */
    public function domains() : array
    {
        return $this->_domains;
    }

    /**
     * Devuelve la ruta completa del archivo donde se almacena la información de los dominios de las traducciones.
     *
     * @return string
     */
    public function filename() : string
    {
        return $this->_directory . '/' . static::dasherized() . '.yaml';
    }

    /**
     * Carga los dominios desde el archivo de entrada.
     *
     * @return bool
     */
    public function load() : bool
    {
        $file           = $this->filename();
        $this->_domains = is_file($file)
            ? $this->loadYaml($file) ?: [ 'messages' => '.' ]
            : [];

        return (bool) $this->_domains;
    }

    /**
     * Guarda el archivo con la información de los dominios encontrados.
     *
     * @param bool $debug Activa el modo depuración.
     *
     * @return bool
     */
    public function save(bool $debug = FALSE) : bool
    {
        ksort($this->_domains);

        return (bool) $this->saveYaml($debug ? 'php://stdout' : $this->filename(), $this->_domains);
    }
}