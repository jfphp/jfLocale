<?php

namespace jf\Locale;

use jf\Base\ISingleton;
use jf\Base\TSingleton;
use Locale as PhpLocale;
use Throwable;

/**
 * Gestiona la localización de aplicaciones usando la extensión `gettext`.
 */
class Locale extends PhpLocale implements ISingleton
{
    use TFormatter;
    use TSingleton;

    /**
     * @inheritdoc
     */
    public const string DEFAULT = 'es_ES';

    /**
     * Separador usado en la RFC4646.
     */
    public const string SEPARATOR_RFC4646 = '-';

    /**
     * Separador usado por `gettext`.
     */
    public const string SEPARATOR_GETTEXT = '_';

    /**
     * Directorio donde se encuentran los archivos con las traducciones.
     *
     * @var string
     */
    private readonly string $_directory;

    /**
     * Idioma a usar.
     *
     * @var string
     */
    private string $_locale = '';

    /**
     * Lista de idiomas disponibles.
     *
     * @var array
     */
    private readonly array $_locales;

    /**
     * Mapa para aquellos idiomas en los cuales el código del país no coincide con la región.
     *
     * @var string[]
     */
    public static array $map = [ 'ca' => 'ES', 'en' => 'GB' ];

    /**
     * Constructor de la clase.
     *
     * @param string $locale    Idioma a usar.
     * @param string $directory Directorio donde se encuentran los archivos con las traducciones.
     */
    public function __construct(string $locale = '', string $directory = '')
    {
        self::$_instance = $this;
        $this->_initDirectory($directory);
        $this->_initDomains();
        $this->_initLocales();
        $this->_initLocale($locale);
    }

    /**
     * Inicializa el directorio y asigna los dominios encontrados dentro.
     *
     * @param string $directory Directorio a inicializar.
     *
     * @return void
     */
    protected function _initDirectory(string $directory) : void
    {
        if (!$directory)
        {
            if (empty($_SERVER['DOCUMENT_ROOT']))
            {
                $dir = getcwd() ?: dirname($_SERVER['PHP_SELF']);
            }
            else
            {
                // Asumimos que la raíz del servidor web se encuentra dentro de un
                // subdirectorio (public, www, etc) del proyecto
                $dir = dirname($_SERVER['DOCUMENT_ROOT']);
            }
            $directory = "$dir/locale";
        }
        $this->_directory = $directory;
    }

    /**
     * Inicializa los dominios encontrados en el directorio con las traducciones.
     *
     * @return void
     */
    protected function _initDomains() : void
    {
        $directory = $this->_directory;
        $domains   = new Domains($directory);
        if ($domains->load())
        {
            $last = '';
            foreach ($domains->domains() as $domain => $subdir)
            {
                if ($subdir[0] !== '/')
                {
                    $subdir = "$directory/$subdir";
                }
                $subdir = realpath($subdir);
                if ($subdir && bindtextdomain($domain, $subdir))
                {
                    bind_textdomain_codeset($domain, 'UTF-8');
                    $last = $domain;
                }
            }
            $main = $domains->default() ?: $last;
            if ($main)
            {
                textdomain($main);
            }
        }
    }

    /**
     * Inicializa el idioma a usar en la ejecución de la aplicación.
     *
     * @param string $locale Idioma a analizar y a asignar.
     *
     * @return void
     */
    protected function _initLocale(string $locale) : void
    {
        if ($locale && $this->isValid($locale))
        {
            $locale = static::toGettext($locale);
        }
        else
        {
            $deflocale = static::toGettext(static::DEFAULT);
            try
            {
                // PHP no devuelve el idioma como lo especifica RFC4646 y hay que cambiar el separador.
                $locale = $this->build(
                    static::lookup(
                        $this->locales(),
                        static::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? $deflocale) ?: $deflocale,
                        FALSE,
                        static::DEFAULT
                    )
                );
                if (!$this->isValid($locale))
                {
                    $locale = $deflocale;
                }
            }
            catch (Throwable)
            {
                $locale = $deflocale;
            }
        }
        $this->set($locale);
    }

    /**
     * Inicializa los idiomas disponibles.
     *
     * @return void
     */
    protected function _initLocales() : void
    {
        $locales = [];
        foreach (glob($this->directory() . '/*', GLOB_ONLYDIR) as $dir)
        {
            $locales[] = $this->build(basename($dir));
        }
        $this->_locales = $locales;
    }

    /**
     * Construye el código completo del idioma.
     *
     * @param string $locale
     *
     * @return string
     */
    public function build(string $locale) : string
    {
        $locale = static::toGettext($locale);
        if (!str_contains($locale, static::SEPARATOR_GETTEXT))
        {
            $locale = strtolower($locale);
            $locale = $locale . static::SEPARATOR_GETTEXT . (self::$map[ $locale ] ?? strtoupper($locale));
        }

        return $locale;
    }

    /**
     * Devuelve el directorio donde se encuentran los archivos con las traducciones.
     *
     * @return string
     */
    public function directory() : string
    {
        return $this->_directory;
    }

    /**
     * Indica si el idioma es válido y está soportado por la aplicación.
     *
     * @param string $locale Idioma a validar.
     *
     * @return bool
     */
    public function isValid(string $locale) : bool
    {
        return $locale && in_array(static::toGettext($locale), $this->locales());
    }

    /**
     * Devuelve el idioma a usar en la ejecución de la aplicación.
     *
     * @return string
     */
    public function locale() : string
    {
        return $this->_locale;
    }

    /**
     * Devuelve el directorio con las traducciones del idioma o una cadena vacía si no existe.
     *
     * @param string $locale Idioma seleccionado.
     *
     * @return string
     */
    public function localeDir(string $locale = '') : string
    {
        return realpath(sprintf('%s/%s', $this->_directory, $locale ? static::toGettext($locale) : $this->locale())) ?: '';
    }

    /**
     * Devuelve los idiomas disponibles.
     *
     * @return string[]
     */
    public function locales() : array
    {
        return $this->_locales;
    }

    /**
     * Asigna el idioma a usar en la ejecución de la aplicación.
     *
     * @param string $locale  Idioma a analizar y a asignar.
     * @param string $charset Juego de carácteres del idioma.
     *
     * @return bool `TRUE` si el idioma es válido y ha sido asignado.
     */
    public function set(string $locale, string $charset = 'UTF-8') : bool
    {
        if ($locale)
        {
            $locale   = $this->build($locale);
            $localech = "$locale.$charset";
            $ok       = $this->isValid($locale) && setlocale(LC_ALL, $localech);
            if ($ok)
            {
                $this->_locale = $locale;
                putenv("LANGUAGE=$localech");
                putenv("LANG=$localech");
                putenv("LC_ALL=$localech");
            }
        }
        else
        {
            $ok = FALSE;
        }

        return $ok;
    }

    /**
     * Formatea el idioma del servidor y lo devuelve tal como lo define RFC4646.
     *
     * @param string $locale Idioma seleccionado.
     *
     * @return string
     */
    public static function toRfc4646(string $locale) : string
    {
        return str_replace(static::SEPARATOR_GETTEXT, static::SEPARATOR_RFC4646, $locale);
    }

    /**
     * Devuelve el idioma tal como se usa en el servidor.
     *
     * @param string $locale Idioma seleccionado.
     *
     * @return string
     */
    public static function toGettext(string $locale) : string
    {
        return str_replace(static::SEPARATOR_RFC4646, static::SEPARATOR_GETTEXT, $locale);
    }
}