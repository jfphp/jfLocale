<?php

namespace jf\Locale;

use DateTimeImmutable;
use IntlDateFormatter;
use MessageFormatter;
use NumberFormatter;

/**
 * Gestiona los formateadores de la extensión `Intl`.
 */
trait TFormatter
{
    /**
     * Caché de formateadores creados.
     *
     * @var array<IntlDateFormatter|MessageFormatter|NumberFormatter>
     */
    private static array $_formatters = [];

    /**
     * Devuelve el formateador de fechas.
     *
     * @param int    $date     Formato de la fecha a usar.
     * @param int    $time     Formato de la hora a usar.
     * @param string $tz       Zona horaria.
     * @param int    $calendar Calendario a usar.
     *
     * @return IntlDateFormatter
     */
    public function dateFormatter(
        int    $date = IntlDateFormatter::LONG,
        int    $time = IntlDateFormatter::NONE,
        string $tz = 'Europe/Madrid',
        int    $calendar = IntlDateFormatter::GREGORIAN
    ) : IntlDateFormatter
    {
        $locale = $this->locale();
        $key    = "dt-$locale-$date-$time-$tz-$calendar";

        return self::$_formatters[ $key ] ??
               (self::$_formatters[ $key ] = IntlDateFormatter::create($locale, $date, $time, $tz, $calendar));
    }

    /**
     * Formatea una fecha.
     *
     * @param DateTimeImmutable $value    Fecha a formatear.
     * @param int               $date     Formato de la fecha a usar.
     * @param int               $time     Formato de la hora a usar.
     * @param string            $tz       Zona horaria.
     * @param int               $calendar Calendario a usar.
     *
     * @return string
     */
    public function formatDate(
        DateTimeImmutable $value,
        int               $date = IntlDateFormatter::LONG,
        int               $time = IntlDateFormatter::NONE,
        string            $tz = 'Europe/Madrid',
        int               $calendar = IntlDateFormatter::GREGORIAN
    ) : string
    {
        $fmt = $this->dateFormatter($date, $time, $tz, $calendar)->format($value);

        return $fmt === FALSE
            ? ''
            : $fmt;
    }

    /**
     * Formatea un texto usando los valores especificados.
     *
     * @param array  $values  Valores a usar para llenar el patrón.
     * @param string $pattern Patrón donde se insertarán los valores.
     *
     * @return string
     */
    public function formatMessage(array $values, string $pattern) : string
    {
        $fmt = $this->messageFormatter($pattern)->format($values);

        return $fmt === FALSE
            ? ''
            : $fmt;
    }

    /**
     * Formatea un valor numérico.
     *
     * @param float|int $fmt      Valor a formatear.
     * @param int       $decimals Cantidad de decimales.
     *
     * @return string
     */
    public function formatNumber(float|int $fmt, int $decimals = 0) : string
    {
        $fmt = $this->numberFormatter($decimals)->format($fmt);

        return $fmt === FALSE
            ? ''
            : $fmt;
    }

    /**
     * Formatea un valor numérico en forma de porcentaje.
     *
     * @param float $fmt      Valor a formatear.
     * @param int   $decimals Cantidad de decimales.
     *
     * @return string
     */
    public function formatPercent(float $fmt, int $decimals = 2) : string
    {
        $fmt = $this->numberFormatter($decimals, NumberFormatter::PERCENT)->format($fmt);

        return $fmt === FALSE
            ? ''
            : $fmt;
    }

    /**
     * @see Locale::locale()
     */
    abstract public function locale() : string;

    /**
     * Devuelve el formateador para textos.
     *
     * @param string $pattern Patrón donde se insertarán los valores.
     *
     * @return MessageFormatter
     */
    public function messageFormatter(string $pattern) : MessageFormatter
    {
        $locale = $this->locale();
        $key    = "mf-$locale-$pattern";

        return self::$_formatters[ $key ] ?? (self::$_formatters[ $key ] = new MessageFormatter($locale, $pattern));
    }

    /**
     * Devuelve el formateador para números.
     *
     * @param int $style    Estilo del formateador.
     * @param int $decimals Cantidad de decimales.
     *
     * @return NumberFormatter
     */
    public function numberFormatter(int $decimals = 0, int $style = NumberFormatter::DECIMAL) : NumberFormatter
    {
        $locale    = $this->locale();
        $key       = "df-$locale-$decimals-$style";
        $formatter = self::$_formatters[ $key ] ?? NULL;
        if (!$formatter)
        {
            $formatter = new NumberFormatter($locale, $style);
            $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, $decimals);
            self::$_formatters[ $key ] = $formatter;
        }

        return $formatter;
    }
}