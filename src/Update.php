<?php

namespace jf\Locale;

use jf\assert\Assert;
use jf\Base\File\TJson;

/**
 * Actualiza los archivos de traducción gestionados por `gettext`.
 */
class Update
{
    use TJson;

    /**
     * Información leída del archivo `composer.json` del proyecto.
     *
     * @var array
     */
    private array $_composer = [];

    /**
     * Directorio con las traducciones del proyecto principal.
     *
     * @var string
     */
    private readonly string $_directory;

    /**
     * Opciones para ejecutar el comando `xgettext`.
     *
     * @var string[]
     */
    public array $options = [
        'indent',
        'force-po',
        'join-existing',
        'foreign-user',
        'no-wrap',
        'sort-output',
        'verbose',
        'default-domain'  => '',
        'files-from'      => '',
        'from-code'       => 'UTF-8',
        'package-name'    => '',
        'package-version' => '',
    ];

    /**
     * Constructor de la clase.
     *
     * @param string $directory Directorio con las traducciones del proyecto principal.
     */
    public function __construct(string $directory)
    {
        Assert::isDir($directory);
        $directory        = realpath($directory);
        $this->_directory = $directory;
        $composer         = dirname($directory) . '/composer.json';
        // Necesitamos que el proyecto haya configurado composer para obtener cierta información desde allí.
        Assert::isFile($composer);
        $this->_composer = $this->loadJson($composer);
    }

    /**
     * Devuelve la información leída del archivo `composer.json` del proyecto.
     *
     * @return array
     */
    public function composer() : array
    {
        return $this->_composer;
    }

    /**
     * Actualiza los archivos de traducciones.
     *
     * @param array $infiles Listado de archivos a examinar en busca de traducciones.
     *
     * @return string[] Listado de archivos generados.
     */
    public function update(array $infiles) : array
    {
        $tmpfile = tempnam('/tmp', 'phpfiles-');
        Assert::notEmpty($tmpfile, 'No se pudo generar el archivo temporal con el listado de archivos a analizar');
        file_put_contents($tmpfile, implode(PHP_EOL, $infiles));
        $composer = $this->composer();
        $name     = $composer['name'] ?? NULL;
        Assert::notEmpty($name, 'Se requiere el nombre del paquete');
        $version = $composer['version'];
        Assert::notEmpty($version, 'Se requiere la versión del paquete');
        $user = $composer['authors'][0] ?? NULL;
        Assert::notEmpty($user, 'Se requiere la información del autor del paquete');
        Assert::arrayKeyExists('name', $user, 'Se requiere el nombre del autor del paquete');
        $username = $user['name'];
        Assert::notEmpty($username, 'Se requiere el nombre del autor del paquete');
        $user = empty($user['email'])
            ? $username
            : sprintf('%s <%s>', $username, $user['email']);
        if (!is_dir('locale'))
        {
            Assert::mkdir('locale', 0o777, TRUE);
        }
        if (count(scandir('locale')) === 2)
        {
            Assert::mkdir('locale/' . Locale::DEFAULT_LOCALE . '/LC_MESSAGES', 0o777, TRUE);
        }
        $langs   = glob('locale/*', GLOB_ONLYDIR);
        $cmd     = 'xgettext';
        $domain  = basename($name);
        $updated = [];
        foreach ($this->options as $option => $value)
        {
            if (is_int($option))
            {
                $option = $value;
                $value  = NULL;
            }
            if ($value === '')
            {
                $value = match ($option)
                {
                    'default-domain'  => $domain,
                    'files-from'      => $tmpfile,
                    'from-code'       => 'UTF-8',
                    'package-name'    => $name,
                    'package-version' => $version,
                    default           => $value
                };
            }
            if ($option[0] !== '-')
            {
                $option = strlen($option) > 1
                    ? "--$option"
                    : "-$option";
            }
            $cmd .= $value === NULL ? " $option" : " $option=$value";
        }
        $directory = $this->_directory;
        foreach ($langs as $lang)
        {
            $lang   = basename($lang);
            $pofile = "$directory/$lang/LC_MESSAGES/$domain.po";
            $outdir = dirname($pofile);
            if (!is_file($pofile))
            {
                if (!is_dir($outdir))
                {
                    Assert::mkdir($outdir, 0o777, TRUE);
                }
                touch($pofile);
            }
            passthru("$cmd --output-dir=" . escapeshellarg($outdir), $code);
            Assert::intIdentical($code, 0, 'Ocurrió un error al generar el archivo de traducción');
            $updated[] = $pofile;
            file_put_contents(
                $pofile,
                preg_replace(
                    '/^.+?msgid/s',
                    <<<TEXT
        #######################################################
        # {$name}:{$version} - $lang
        #######################################################
        
        msgid
        TEXT,
                    strtr(
                        preg_replace(
                            [ '/(PO-Revision-Date:).+?(\\\\n)/m', '|#,\s+php-format\n|' ],
                            [ '$1 ' . date('Y-m-d H:i:sO') . '$2', '' ],
                            file_get_contents($pofile) ?: '',
                        ) ?: '',
                        [
                            'Content-Type: text/plain; charset=CHARSET\n'          => 'Content-Type: text/plain; charset=UTF-8\n',
                            'Language: \n'                                         => "Language: $lang\\n",
                            'Language-Team: LANGUAGE <LL@li.org>\n'                => "Language-Team: $user\\n",
                            'Last-Translator: FULL NAME <EMAIL@ADDRESS>\n'         => "Last-Translator: $user\\n",
                            'Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n' => "Plural-Forms: nplurals=3; plural=(n > 1 ? 2 : n);\\n",
                            'Report-Msgid-Bugs-To: \n'                             => "Report-Msgid-Bugs-To: $user\\n"
                        ]
                    )
                )
            );
        }
        unlink($tmpfile);

        return $updated;
    }
}